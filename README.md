# Testia

Application réalisé pour un test technique pour Testia.


## Application Back-end
Lancement de l'application à travers le script server.py. 

Version Python3.7, avec les libs MySQL, Poney, Flask.

L'application généré les tables nécessaire en BDD, configuration des identifiants dans le fichier config.py


## Application Front

Pour le moment le déploiment est uniquement disponible vers le déploiment classique Angular.

Suivre les instruction dans le README.md
