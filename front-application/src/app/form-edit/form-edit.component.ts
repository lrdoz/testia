import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';


import { Form }         from '../form/form.model';
import { FormService }  from '../form/form.service';

@Component({
  selector: 'ns-form-edit',
  templateUrl: './form-edit.component.html',
  styleUrls: ['./form-edit.component.css']
})


export class FormEditComponent implements OnInit {
	
  form: Form;
  ref: string;

  profileForm: FormGroup;

  defaults = [['Délaminage', 'Peeling'], 
  			  ['Porosité', 'Porosity'], 
			  ['Inclusion', 'Inclusion'], 
			  ['Autre', 'Other'] ];
  
  constructor( 
    private route: ActivatedRoute,
	private formService: FormService,
	private location: Location){
  }

  ngOnInit() {
	this.form = new Form();
	this.definedFormGroup();
	this.getForm();
  }

  onSubmit(){
	let values = this.profileForm.value;
	this.form.operatorName = values.operatorName;
	this.form.defaultType = values.defaultType;
	this.form.complementaryInformation = values.complementaryInformation;
	this.formService.saveForm(this.form).subscribe( _ =>this.location.back());
  }

  getForm(){
	this.ref = this.route.snapshot.paramMap.get('ref');
	if (this.ref){
		this.formService.getFormByRef(this.ref).subscribe(data => {this.form = data; this.definedFormGroup();});
	}

  }

  definedFormGroup(){

	this.profileForm = new FormGroup({
				  'operatorName': new FormControl(this.form.operatorName, [Validators.required, Validators.pattern("^([^0-9]*)$")]),
			      'defaultType': new FormControl(this.form.defaultType, [Validators.required]),
			      'complementaryInformation': new FormControl(this.form.complementaryInformation),
				 });
  }

  get operatorName() { return this.profileForm.get('operatorName'); }


}
