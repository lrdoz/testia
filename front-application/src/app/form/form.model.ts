export class Form {
	ref: string;
	defaultType: string;
	operatorName: string;
	complementaryInformation: string;
	creationDate: string;
	lastUpdateDate: string;

	constructor(){
		this.ref="";
	}
}

export class Forms {
	allForm: Form[];
}
