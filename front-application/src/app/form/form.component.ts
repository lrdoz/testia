import { Component, OnInit } from '@angular/core';
import { FormService } from './form.service';
import { Form} from './form.model';

@Component({
  selector: 'ns-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {
  
  forms$: Form[];

  constructor(private formService: FormService){}

	
  toggleRemove(ref: string){
	this.formService.deleteFormByRef(ref).subscribe();
	 this.getAllForms();
  }

  ngOnInit() {
	 this.getAllForms();
  }
	
  getAllForms(){
	this.formService.getForms().subscribe(data => this.forms$ = data.allForm); 
  }
  
}
