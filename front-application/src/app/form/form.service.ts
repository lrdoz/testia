import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Form, Forms } from './form.model';

@Injectable({
  providedIn: 'root'

})

export class FormService{

  url="https://testia.lrdoz.com/api/form/";

  constructor(private http: HttpClient) { }

  getForms(){
    return this.http.get<Forms>(this.url);
  }

  getFormByRef(ref: string){
    return this.http.get<Form>(this.url + ref);
  }

  deleteFormByRef(ref: string){
    return this.http.delete(this.url + ref);
  }

  saveForm(form: Form){
	return this.http.post<Form>(this.url, JSON.stringify(form));
  }
}
