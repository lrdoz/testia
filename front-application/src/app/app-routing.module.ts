import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FormComponent }   from './form/form.component';
import { FormEditComponent }      from './form-edit/form-edit.component';

const routes: Routes = [
 	{ path: '', redirectTo: '/forms', pathMatch: 'full' },
    { path: 'forms', component: FormComponent},
	{ path: 'edit/:ref', component: FormEditComponent},
	{ path: 'edit', component: FormEditComponent},
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})

export class AppRoutingModule {}
