from random import randint
from datetime import datetime



class ServiceForm ():

	"""
	Class defined service form


	"""
	def __init__(self, formRepository):
		self.__formReposiroty = formRepository


	def findByRef(self, ref):
		"""
		Retrieve form with ref

		:param ref: Reference of form
		"""
		return self.__formReposiroty.findFormByRef(ref)

	def getAllForm(self):
		"""
		Retrieve all form in database
		"""
		return self.__formReposiroty.getAllForm()
	
	def createForm(self, form):
		"""
		Create form

		:parma form: Form create
		"""
		ref = str(randint(10000, 99999))
		form.ref = ref
		form.creationDate = datetime.now()
		form.lastUpdateDate = datetime.now()

		return self.__formReposiroty.createForm(form)
	

	def updateForm(self, form):
		"""
		Update form with the reference 

		:param form: Form update
		"""
		form.lastUpdateDate = datetime.now()
		return self.__formReposiroty.updateForm(form)

	def deleteForm(self, ref):
		"""
		Remove form with the reference

		:param ref: Remove form
		"""
		self.__formReposiroty.deleteFormByRef(ref)	
