from pony.orm import *
from pony import orm
from Form import Form
from datetime import datetime

db = Database()



class FormEntity(db.Entity):
	"""
	Defined entity in dataBase for form entity
	"""
	_table_ ="form"
	id = PrimaryKey(int, auto = True) 
	ref = Required(str, unique = True) 
	default_type = Required(str)
	operator_name= Required(str)
	complementary_information = Optional(str)
	creation_date = Required(datetime)
	last_update_date = Required(datetime)


	def convertToForm(self):
		"""
		Convert EntityForm to Form
		"""
		newForm = Form()
		newForm.convertEntityFormToForm(self)

	def updateFormValue(self, form):
		"""
		Permit to map form into FormEnitty

		:param form: Form would map
		"""
		self.default_type = form.defaultType.value
		self.operator_name = form.operatorName
		self.complementary_information = form.complementaryInformation
		self.creation_date = form.creationDate
		self.last_update_date = form.lastUpdateDate

	@staticmethod
	def convertFormToEntityForm(form):
		"""
		Generate form in data Base

		Warm when creat entityForm, that generate transaction for create form
		:return: ForEntity Generate
		"""
		newEntityForm = FormEntity(ref = form.ref, \
					default_type = form.defaultType.value, \
					operator_name = form.operatorName, \
					complementary_information = form.complementaryInformation, \
					creation_date = form.creationDate, \
					last_update_date = form.lastUpdateDate)
		return newEntityForm.convertEntityFormToForm()

	def convertToForm(self):
		"""
		Convert Entity Form to Form


		:return: Form equivalence
		"""

		newForm = Form()
		newForm.ref = self.ref
		newForm.defaultType = self.default_type
		newForm.operatorName = self.operator_name
		newForm.complementaryInformation = self.complementary_information
		newForm.creationDate = self.creation_date
		newForm.lastUpdateDate = self.last_update_date
		
		return newForm

class FormRepository():
	"""
	Defined all dataBase transaction can be make for form object
	"""

	@db_session
	def updateForm(self, form):
		"""
		Update Form with reference defined

		:param form: New form state
		:return: Update form in database
		"""

		#Generate transaction for data base
	
		entityForm = FormEntity.get(ref =form.ref)
		entityForm.updateFormValue(form)
		return entityForm.convertToForm()

	@db_session
	def createForm(self, form):
		"""
		Create new form in database

		:param form: New form add
		:return: The new form add in database
		"""
		entityForm = FormEntity.convertFormToEntityForm(form)

		return entityForm.convertToForm()

	def findFormByRef(self, ref):
		"""
		Find form with this reference

		:param ref: Reference of form
		:return: Form find 
		"""
		formEntityFind = FormEntity.get(ref = ref)

		if formEntityFind :
			return formEntityFind.convertToForm()
		else:
			return None

	def getAllForm(self):
		"""
		Retrieve all form in database

		:return: all Forms retrieve
		"""

		listOfAllFormEntity = FormEntity.select()
		return list(map(lambda form : form.convertToForm(), listOfAllFormEntity))
	
	@db_session
	def deleteFormByRef(self, ref):
		"""
		Reference remove in database

		:param formEntity: Reference of form
		"""
		formEntity = FormEntity.get(ref = ref)

		if formEntity :
			formEntity.delete()
