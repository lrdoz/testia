from flask import Flask
from flask import request, jsonify
from flask_login import LoginManager

from FormRepository import *
from ServiceForm import *
from Form import *

from pony.orm import *
from pony import orm
from pony.flask import Pony
from config import config
from FormRepository  import db
from werkzeug.exceptions import HTTPException

import json

#Initialisation service form
formService = ServiceForm(FormRepository())

app = Flask(__name__)
app.config.update(config)

db.bind(**app.config['PONY'])
db.generate_mapping(create_tables=True)
Pony(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'


NOT_FOUND_EXCEPTION = {"error": "404 Not Found: The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again."}

@login_manager.user_loader
def load_user(user_id):
    return db.User.get(id=user_id)

	
@app.route('/api/form/<ref>', methods=['GET'])
def getFormByRef(ref):
	"""
	Get form with the reference 

	:param ref: Reference of form
	:return: Form find in database
	"""
	
	form = formService.findByRef(ref)

	if form :
		return form.__dict__
	else:
		return NOT_FOUND_EXCEPTION

@app.route('/api/form/', methods=['GET'])
def getAllForm():
	"""
	Retrieve all form

	:return: All form find
	"""

	
	allForm = formService.getAllForm()

	allForm = list(map(lambda form: form.__dict__, allForm))
	return jsonify(allForm=allForm)

@app.route('/api/form/', methods=['POST'])
def saveForm():
	"""
	Update or create form service

	:return: Form save 
	"""
	data = json.loads(request.get_data().decode())
	

	form = Form()
	form.integrateDict(data)

	form.defaultType = Defect(form.defaultType)
	if form.ref != "":
		form = formService.updateForm(form)
	else:
		form = formService.createForm(form)

	return form.__dict__


@app.route('/api/form/<ref>', methods=['DELETE'])
def removeFormByRef(ref):
	"""
	Remove form service 
		
	:param ref: Reference of form removed
	"""
	formService.deleteForm(ref)

	return {}

@app.errorhandler(Exception)
def handle_error(e):
	return NOT_FOUND_EXCEPTION
