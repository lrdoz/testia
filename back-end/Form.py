from enum import Enum
from datetime import datetime

class Defect(Enum):
	"""
	Defect can be report by operator 
	"""
	PEELING= "Peeling" # Délaminage
	POROSITY = "Porosity" # Porosité
	INCLUSION = "Inclusion" # Inclusion
	OTHER = "Other" # Autre


class Form():
	"""
	Class defined form object
	"""
	
	def __init__(self):
		"""
		Initialize form attribute		
		"""
		self.ref = None
		self.defaultType = None
		self.operatorName= None
		self.complementaryInformation = None
		self.creationDate = None
		self.lastUpdateDate = None

	def integrateDict(self, formData):
		"""
		Set all keys on attribute class

		:param formData: Dict with attribute map into class
		"""

		for key, value in formData.items():
			setattr(self, key, value)
